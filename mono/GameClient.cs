﻿using System.Collections.Concurrent;
using System.ComponentModel;
using System.Threading;
using HelloWorldOpen.Models;
using HelloWorldOpen.Models.Client;
using HelloWorldOpen.Models.Client.Messages;
using HelloWorldOpen.Models.EventArguments;
using HelloWorldOpen.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldOpen
{
    #region Event delegates
    /// <summary>
    /// Informs the listeners that the client status.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
    public delegate void GameStatusEventHandler(object sender, GameClientEventArgs args);

    /// <summary>
    /// Alerts the listeners to receive the initial data for the race session.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
    public delegate void RaceInitializationHandler(object sender, GameClientEventArgs args);

    /// <summary>
    /// Alerts the listeners to receive the updated data for the cars in the race session.
    /// </summary>
    /// <param name="sender">The sender.</param>
    /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
    public delegate void GameTickUpdateHandler(object sender, GameClientEventArgs args);
    #endregion

    /// <summary>
    /// Game client is used for communication between the bot logic and the game server.
    /// </summary>
    public class GameClient
    {
        #region Variables
        private readonly Logger _logger;

        private readonly BackgroundWorker _bw;

        public event GameStatusEventHandler GameStatusChanged;
        public event RaceInitializationHandler RaceInitialized;
        public event GameTickUpdateHandler GameTickUpdated;

        private readonly StreamReader _reader;
        private readonly StreamWriter _writer;
        private readonly ConcurrentQueue<MessageCapsule> _messageQueue;

        private string _racerName;
        private bool _raceRunning;
        private bool _gameCreated;
        #endregion

        #region Constructor and worker methods

        /// <summary>
        /// Initializes a new instance of the <see cref="GameClient"/> class.
        /// </summary>
        /// <param name="name">Name of the racer.</param>
        /// <param name="host">The host.</param>
        /// <param name="port">The port.</param>
        public GameClient(string name, string host, int port)
        {
            // Create logger
            _logger = new Logger(".\\logs\\" + name + "_sessionlog_gameclient");
            // Initialize variables
            _gameCreated = false;
            _racerName = name;
            // Create TCP Client for connection
            TcpClient client = new TcpClient(host, port);
            // Initialize stream objects
            NetworkStream stream = client.GetStream();
            _reader = new StreamReader(stream);
            _writer = new StreamWriter(stream) { AutoFlush = true };
            // Initialize message queue
            _messageQueue = new ConcurrentQueue<MessageCapsule>();
            // Create background worker for TCP communication
            _bw = new BackgroundWorker { WorkerSupportsCancellation = true };
            _bw.DoWork += client_DoWork;
        }

        /// <summary>
        /// Starts this instance.
        /// </summary>
        public void Start()
        {
            if (!_bw.IsBusy)
                _bw.RunWorkerAsync();
        }

        /// <summary>
        /// Handles the DoWork event of the game client.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="doWorkEventArgs">The <see cref="DoWorkEventArgs"/> instance containing the event data.</param>
        private void client_DoWork(object sender, DoWorkEventArgs doWorkEventArgs)
        {
            string line;

            // Handle received data
            while ((line = _reader.ReadLine()) != null)
            {
                // Log received message
                _logger.WriteLine("IN " + line);

                // Process incoming messages
                MessageWrapper msg = JsonConvert.DeserializeObject<MessageWrapper>(line);
                switch (msg.msgType)
                {
                    case "error":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Error, ArgumentData = msg });
                        break;
                    // Server confirmed race creation
                    case "createRace":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Created });
                        break;
                    // Server confirmed join-command
                    case "join":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Joined });
                        break;
                    // Server confirmed joinRace-command
                    case "joinRace":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Joined });
                        break;
                    // Server sent initial race data
                    case "gameInit":
                        // Queue initialization event and data to listening objects
                        InitializeRace(msg);
                        break;
                    // Server started the game
                    case "gameStart":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Started });
                        break;
                    // Server sent updated car positions
                    case "carPositions":
                        UpdateGameTick(msg);
                        break;
                    // Bot crashed
                    case "crash":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Crash, ArgumentData = msg });
                        break;
                    // Bot respawned
                    case "spawn":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Spawn, ArgumentData = msg });
                        break;
                    // Bot was disqualified
                    case "dnf":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Disqualified, ArgumentData = msg });
                        break;
                    // Bot finished a lap
                    case "lapFinished":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.LapFinished, ArgumentData = msg });
                        break;
                    // Bot car finished the race
                    case "finish":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Finished, ArgumentData = msg });
                        break;
                    // Server ended the game
                    case "gameEnd":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.Ended });
                        break;
                    case "tournamentEnd":
                        UpdateGameStatus(new GameClientEventArgs() { GameState = GameState.TournamentEnd });
                        break;
                    default:
                        _logger.WriteLine("UNKNOWN " + ObjectDumper.Dump(msg));
                        Send(new Ping());
                        break;
                }
            }
        }
        #endregion

        #region Event handlers
        /// <summary>
        /// Updates the client status to listeners.
        /// </summary>
        private void UpdateGameStatus(GameClientEventArgs args)
        {
            if (args.GameState == GameState.Created)
            {
                _gameCreated = true;
            }

            if (GameStatusChanged != null)
                GameStatusChanged(this, args);

            Send(new Ping());
        }

        /// <summary>
        /// Gets the race data sent by server and triggers race data update event.
        /// </summary>
        /// <param name="msg">Race data as JObject.</param>
        private void InitializeRace(MessageWrapper msg)
        {
            if (RaceInitialized != null)
                RaceInitialized(this, new GameClientEventArgs() { ArgumentData = ((JObject)msg.data)["race"].ToObject<Race>() });

            Send(new Ping());
        }

        /// <summary>
        /// Updates the car positions from data sent by server.
        /// </summary>
        /// <param name="gameState">Updated game state data.</param>
        private void UpdateGameTick(MessageWrapper gameState)
        {
            if (GameTickUpdated != null)
                GameTickUpdated(this, new GameClientEventArgs() { ArgumentData = gameState });

            if (!_messageQueue.IsEmpty && _raceRunning)
            {
                MessageCapsule message;
                _messageQueue.TryDequeue(out message);
                if (message != null) Send(message);
            }
            else
            {
                Send(new Ping());
            }

            if (!_raceRunning) _raceRunning = true;
        }
        #endregion

        #region Client functionality
        /// <summary>
        /// Sends the specified MSG to the server.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        private void Queue(MessageCapsule msg)
        {
            _messageQueue.Enqueue(msg);
        }

        /// <summary>
        /// Sends the specified MSG to the server.
        /// </summary>
        /// <param name="msg">The MSG.</param>
        private void Send(MessageCapsule msg)
        {
            // Log sent message
            _logger.WriteLine("OUT " + msg.ToJson());
            // Send to server
            _writer.WriteLine(msg.ToJson());
        }

        #region Game controls
        /// <summary>
        /// Creates a private game with password.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="key">The key.</param>
        /// <param name="track">The track.</param>
        /// <param name="numCars">The number cars.</param>
        public bool CreateGame(string name, string key, string track, int numCars)
        {
            Send(new CreateRace(name, key, track, numCars));
            Start();

            while (true)
            {
                if (_gameCreated) break;
                System.Threading.Thread.Sleep(1);
            }

            return true;
        }

        /// <summary>
        /// Creates a private game with password.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="key">The key.</param>
        /// <param name="track">The track.</param>
        /// <param name="password">The password.</param>
        /// <param name="numCars">The number cars.</param>
        public bool CreateGame(string name, string key, string track, string password, int numCars)
        {
            Send(new CreateRace(name, key, track, password, numCars));
            Start();

            while (true)
            {
                if (_gameCreated) break;
                System.Threading.Thread.Sleep(1);
            }

            return true;
        }

        /// <summary>
        /// Fast joins a car to a solo game.
        /// </summary>
        /// <param name="name">The driver name.</param>
        /// <param name="key">The key.</param>
        public void JoinGame(string name, string key)
        {
            Send(new FastJoin(name, key));
            Start();
        }

        /// <summary>
        /// Joins a specific game.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="key">The key.</param>
        /// <param name="numCars">The number cars.</param>
        public void JoinGame(string name, string key, string track, int numCars)
        {
            Send(new JoinRace(name, key, numCars));
            Start();
        }

        /// <summary>
        /// Joins a specific game.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="key">The key.</param>
        /// <param name="track">The track.</param>
        /// <param name="password">The password.</param>
        /// <param name="numCars">The number cars.</param>
        public void JoinGame(string name, string key, string track, string password, int numCars)
        {
            Send(new JoinRace(name, key, track, password, numCars));
            Start();
        }
        #endregion

        #region Car controls
        /// <summary>
        /// Sets the throttle.
        /// </summary>
        /// <param name="value">The value.</param>
        public void SetThrottle(double value)
        {
            Queue(new Throttle(value));
        }

        /// <summary>
        /// Sets the throttle to change on specific game tick.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="tick">The game tick.</param>
        public void SetThrottle(double value, int tick)
        {
            Queue(new Throttle(value, tick));
        }

        internal void SwitchLanes(Direction direction)
        {
            Queue(new SwitchLane(direction));
        }
        #endregion

        #endregion
    }
}
