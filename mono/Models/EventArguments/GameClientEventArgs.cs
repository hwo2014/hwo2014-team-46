using System;
using HelloWorldOpen.Models.Client;

namespace HelloWorldOpen.Models.EventArguments
{
    /// <summary>
    /// EventArgument class for <see cref="GameClient"/> events.
    /// </summary>
    public class GameClientEventArgs : EventArgs
    {
        public GameState GameState { get; set; }
        public Object ArgumentData { get; set; }
    }
}