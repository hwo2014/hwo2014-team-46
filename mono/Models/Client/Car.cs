﻿namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to define variables for a single racecar.
    /// 
    /// <see cref="CarIdentity" value="ID"/> - Contains the identification variables for the car.
    /// <see cref="CarDimensions" value="CarDimensions"/> - Contains the 2D dimension data for the car.
    /// Angle - Contains the current slip angle of the car.
    /// <see cref="CarPosition" value="Piece Position"/> - Contains the location and lap data for the car.
    /// </summary>
    public class Car
    {
        public CarIdentity id { get; set; }
        public CarDimensions CarDimensions { get; set; }
        public double angle { get; set; }
        public CarPosition piecePosition { get; set; }
    }
}
