﻿namespace HelloWorldOpen.Models.Client
{

    /// <summary>
    /// Enum to representate game state.
    /// </summary>
    public enum GameState
    {
        Error,
        Created,
        Joined,
        Started,
        Ended,
        Crash,
        Spawn,
        Disqualified,
        LapFinished,
        Finished,
        TournamentEnd
    }
}
