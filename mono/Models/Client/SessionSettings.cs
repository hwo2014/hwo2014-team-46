namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to define settings for a race session.
    /// </summary>
    public class SessionSettings
    {
        public int laps { get; set; }
        public int maxLapTimeMs { get; set; }
        public bool quickRace { get; set; }
    }
}
