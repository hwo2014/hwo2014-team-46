﻿using System.Collections.Generic;

namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to contain all the data for a race session.
    /// </summary>
    public class Race
    {
        public Track track { get; set; }
        public List<Car> cars { get; set; }
        public SessionSettings raceSession { get; set; }
    }
}