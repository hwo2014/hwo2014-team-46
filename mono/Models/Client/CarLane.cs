﻿namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Contains the lane data for the race car. Index only differs on switch tracks
    /// where end lane index is the index of the lane the car is switching to.
    /// </summary>
    public class CarLane
    {
        public int startLaneIndex { get; set; }
        public int endLaneIndex { get; set; }
    }
}