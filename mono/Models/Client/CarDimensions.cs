﻿namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Contains the 2D CarDimensions for the racecar.
    /// </summary>
    public class CarDimensions
    {
        public double length { get; set; }
        public double width { get; set; }
        public double guideFlagPosition { get; set; }
    }
}