namespace HelloWorldOpen.Models.Client.Messages
{
    class Ping : MessageCapsule
    {
        protected override string MsgType()
        {
            return "ping";
        }
    }
}