using System;

namespace HelloWorldOpen.Models.Client.Messages
{
    class SwitchLane : MessageCapsule
    {
        public Direction direction;

        public SwitchLane(Direction direction)
        {
            this.direction = direction;
        }

        protected override Object MsgData()
        {
            return this.direction.ToString();
        }

        protected override string MsgType()
        {
            return "switchLane";
        }
    }
}
