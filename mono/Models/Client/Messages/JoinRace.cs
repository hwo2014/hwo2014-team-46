namespace HelloWorldOpen.Models.Client.Messages
{
    class JoinRace : MessageCapsule
    {
        public BotId botId;
        public string trackName { get; set; }
        public string password { get; set; }
        public int carCount { get; set; }

        public JoinRace(string botName, string botKey, int numCars)
        {
            botId = new BotId() { name = botName, key = botKey };
            carCount = numCars;
        }

        public JoinRace(string botName, string botKey, string track, string racePassword, int numCars)
        {
            botId = new BotId() { name = botName, key = botKey };
            trackName = track;
            password = racePassword;
            carCount = numCars;
        }

        protected override string MsgType()
        {
            return "joinRace";
        }
    }
}