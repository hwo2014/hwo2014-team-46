using System;

namespace HelloWorldOpen.Models.Client.Messages
{
    class Throttle : MessageCapsule
    {
        public double value;
        public int? tick;

        public Throttle(double value)
        {
            this.value = value;
        }

        public Throttle(double value, int tick)
        {
            this.value = value;
            this.tick = tick;
        }

        protected override Object MsgData()
        {
            return this.value;
        }

        protected override string MsgType()
        {
            return "throttle";
        }

        protected override int? GameTick()
        {
            return this.tick;
        }
    }
}