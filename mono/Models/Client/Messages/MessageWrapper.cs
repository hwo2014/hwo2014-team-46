﻿namespace HelloWorldOpen.Models.Client.Messages
{
    public class MessageWrapper
    {
        public string msgType { get; set; }
        public object data { get; set; }
        public string gameId { get; set; }
        public int? gameTick { get; set; }
    }
}
