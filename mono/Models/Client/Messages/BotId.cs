namespace HelloWorldOpen.Models.Client.Messages
{
    internal class BotId
    {
        public string name { get; set; }
        public string key { get; set; }
    }
}