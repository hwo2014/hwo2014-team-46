using System;
using Newtonsoft.Json;

namespace HelloWorldOpen.Models.Client.Messages
{
    abstract class MessageCapsule
    {
        public string ToJson()
        {
            return JsonConvert.SerializeObject(new MessageWrapper() { msgType = MsgType(), data = MsgData(), gameId = GameId(), gameTick = GameTick() }, Formatting.None, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
        }

        protected virtual Object MsgData()
        {
            return this;
        }

        protected abstract string MsgType();

        protected virtual string GameId()
        {
            return null;
        }

        protected virtual int? GameTick()
        {
            return null;
        }
    }
}
