namespace HelloWorldOpen.Models.Client.Messages
{
    class FastJoin : MessageCapsule
    {
        public string name;
        public string key;
        public string color;

        public FastJoin(string name, string key)
        {
            this.name = name;
            this.key = key;
            this.color = "red";
        }

        protected override string MsgType()
        {
            return "join";
        }
    }
}