namespace HelloWorldOpen.Models.Client.Messages
{
    class CreateRace : MessageCapsule
    {
        public BotId botId;
        public string trackName { get; set; }
        public string password { get; set; }
        public int carCount { get; set; }

        public CreateRace(string botName, string botKey, string track, int numCars)
        {
            botId = new BotId() { name = botName, key = botKey };
            trackName = track;
            carCount = numCars;
        }

        public CreateRace(string botName, string botKey, string track, string racePassword, int numCars)
        {
            botId = new BotId() { name = botName, key = botKey };
            trackName = track;
            password = racePassword;
            carCount = numCars;
        }

        protected override string MsgType()
        {
            return "createRace";
        }
    }
}