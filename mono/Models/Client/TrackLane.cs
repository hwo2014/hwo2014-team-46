namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to define race track lane positioning.
    /// </summary>
    public class TrackLane
    {
        public int distanceFromCenter { get; set; }
        public int index { get; set; }
    }
}