namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to define the position of the first track piece.
    /// 
    /// Position - Track piece on the track.
    /// Angle - Angle the car is facing initially.
    /// </summary>
    public class StartingPoint
    {
        public Position position { get; set; }
        public double angle { get; set; }
    }
}