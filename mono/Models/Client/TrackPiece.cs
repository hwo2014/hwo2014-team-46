namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to describe the properties of a piece of track.
    /// 
    /// For a straight lane the object contains only the value length
    /// and optionally for switch tracks the boolean value true for switch.
    /// 
    /// For a bend the object contains the rounded degree for angle and radius.
    /// 
    /// Bridge variable is for a visual representation of a track piece on a bridge.
    /// This variable does not affect the actual gameplay.
    /// </summary>
    public class TrackPiece
    {
        public double length { get; set; }
        public bool @switch { get; set; }
        public bool bridge { get; set; }
        public double angle { get; set; }
        public int radius { get; set; }
    }
}