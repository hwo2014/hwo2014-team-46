namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Position for a track piece on the grid.
    /// </summary>
    public class Position
    {
        public double x { get; set; }
        public double y { get; set; }
    }
}
