namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Contains the identification for the car.
    /// </summary>
    public class CarIdentity
    {
        public string name { get; set; }
        public string color { get; set; }
    }
}