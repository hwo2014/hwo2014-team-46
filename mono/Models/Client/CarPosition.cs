﻿namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Contains the position and lap data for the race car.
    /// </summary>
    public class CarPosition
    {
        public int pieceIndex { get; set; }
        public double inPieceDistance { get; set; }
        public CarLane lane { get; set; }
        public int lap { get; set; }
    }
}