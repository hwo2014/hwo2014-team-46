using System.Collections.Generic;

namespace HelloWorldOpen.Models.Client
{
    /// <summary>
    /// Class to contain track data for a single race.
    /// </summary>
    public class Track
    {
        public string id { get; set; }
        public string name { get; set; }
        public List<TrackPiece> pieces { get; set; }
        public List<TrackLane> lanes { get; set; }
        public StartingPoint startingPoint { get; set; }
    }
}