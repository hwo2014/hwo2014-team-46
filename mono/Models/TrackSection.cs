﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldOpen.Models
{
    public class TrackSection
    {
        public SectionType SectionType { get; set; }
        public List<SectionLane> SectionLanes { get; set; } 
    }

    public enum SectionType
    {
        Straight,
        Corner,
        Switch,
        CornerSwitch
    }

    public class SectionLane
    {
        public double Length { get; set; }
    }
}
