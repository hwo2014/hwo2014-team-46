﻿using System;
using System.IO;

namespace HelloWorldOpen.Utils
{
    public class Logger
    {
        private readonly TextWriter _writer;

        public Logger(string path)
        {
            path = path + '_' + DateTime.Now.ToString("HH_mm_ss_dd_MM_yyyy") + ".txt";
            _writer = new StreamWriter(path, true);
        }

        public void Write(string message)
        {
            _writer.Write(message);
        }

        public void WriteLine(string line)
        {
            _writer.WriteLine(line);
        }
    }
}
