﻿using System.ComponentModel;
using System;
using System.Collections.Generic;

namespace HelloWorldOpen.Utils
{
    public class Diagnostics
    {
        private static bool _diagnosticOutput = false;
        private static int _botIndex;
        private static readonly List<Bot> BotList = new List<Bot>();
        private static readonly List<string> NotificationQueue = new List<string>(); 

        public static void SetupDashboard()
        {
            _diagnosticOutput = true;

            Console.CursorVisible = false;
            Console.SetBufferSize(Console.WindowWidth, Console.WindowHeight);
            Console.Clear();
            Console.SetCursorPosition(0, 0);
            Console.Write("Connected");
            Console.SetCursorPosition(0, 1);

            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += UpdateConsole;
            bw.RunWorkerAsync();
        }

        public static void ChangeBotDisplay(int indexDifference)
        {
            if (indexDifference < 0)
            {
                if (_botIndex - 1 < 0) return;
                _botIndex--;
            }
            else
            {
                if (_botIndex == BotList.Count - 2) return;
                _botIndex++;
            }
        }

        private static void UpdateConsole(object sender, DoWorkEventArgs e)
        {
            _botIndex = 0;

            while (true)
            {
                if (BotList.Count > 0)
                {
                    var bot = BotList[_botIndex];
                    PrintStatus(_botIndex, bot.GameTick != null && bot.GameTick % 2 == 0 ? '*' : '-');
                }
            }
        }

        private static string GenerateWhitespace(int amount)
        {
            string whitespace = "";

            for (int i = 0; i < amount; i++)
            {
                whitespace += " ";
            }

            return whitespace;
        }

        private static void ClearAndGoToConsoleLine(int lineNumber)
        {
            Console.SetCursorPosition(0, lineNumber);
            Console.Write(GenerateWhitespace(Console.WindowWidth));
            Console.SetCursorPosition(0, lineNumber);
        }

        private static void PrintStatus(int botIndex, char indicator)
        {
            var bot = BotList[botIndex];
            if (bot.Car == null)
                return;

            Console.SetCursorPosition(0, 0);

            List<KeyValuePair<string, string>> diagnosticRows = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>(bot.Name + ' ' + indicator, ""),
                new KeyValuePair<string, string>("GameTick: ", bot.GameTick.ToString()),
                new KeyValuePair<string, string>("GameState: ", bot.GameState.ToString()),
                new KeyValuePair<string, string>("Throttle: ", bot.Throttle.ToString()),
                new KeyValuePair<string, string>("Angle: ", bot.Car.angle.ToString()),
                new KeyValuePair<string, string>("Current piece index: ", bot.Car.piecePosition.pieceIndex.ToString()),
                new KeyValuePair<string, string>("Distance travelled on current piece: ", bot.Car.piecePosition.inPieceDistance.ToString())
            };

            foreach (KeyValuePair<string, string> messagePair in diagnosticRows)
            {
                Console.WriteLine(messagePair.Key);
                ClearAndGoToConsoleLine(Console.CursorTop);
                Console.WriteLine(messagePair.Value + '\n');
            }

            if (NotificationQueue.Count > 0)
            {
                ClearAndGoToConsoleLine(Console.WindowHeight - 3);
                Console.WriteLine(NotificationQueue[0]);
                NotificationQueue.RemoveAt(0);
            }
        }

        public static void Register(Bot bot)
        {
            BotList.Add(bot);
        }

        public static void Notify(string message)
        {
            if (_diagnosticOutput) NotificationQueue.Add(message);
            else Console.WriteLine(message);
        }
    }
}
