﻿using System.ComponentModel;
using System.Runtime.InteropServices;
using HelloWorldOpen.Models;
using HelloWorldOpen.Models.Client;
using HelloWorldOpen.Models.Client.Messages;
using HelloWorldOpen.Models.EventArguments;
using HelloWorldOpen.Utils;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace HelloWorldOpen
{
    /// <summary>
    /// Event delegate to inform the end of the tournament.
    /// </summary>
    /// <param name="sender">The sender.</param>
    public delegate void TournamentEndHandler(object sender);

    /// <summary>
    /// Handles Car AI logic
    /// </summary>
    public class Bot
    {
        #region Variables
        private readonly Logger _logger;
        private readonly GameClient _client;
        public event TournamentEndHandler TournamentEnded;

        public string Name { get; private set; }
        private readonly string _accessKey;

        public GameState GameState { get; private set; }
        public int? GameTick { get; private set; }
        public Race Race { get; private set; }
        public Car Car { get; private set; }
        public List<TrackSection> TrackSections { get; private set; } 

        private double _throttle;
        public double Throttle
        {
            get { return _throttle; }
            private set
            {
                if (Math.Abs(_throttle - value) < 0) return;

                if (value < 0.0 || value > 1.0) return;

                _throttle = value;

                if (GameTick < 1)
                {
                    SetThrottle(value, 1);
                }
                else
                {
                    SetThrottle(value);
                }
            }
        }

        private int _currentPiece = 0;
        private int _nextPiece = 0;
        private int _nextPiece2 = 0;
        private int _lastPieceId = 0;

        private double _frictionNumber = 0.35;

        private bool _switchingTracks = false;
        private int _targetTrackLane = -1;
        private bool _botAlive;

        private double _lastSpeed;
        private double _currentSpeed;
        private double _lastAcceleration;
        private double _currentAcceleration;

        private double _lastinPieceDistance;
        private bool _switchedLanes = false;
        private bool _switchedIn = false;
        private double _laneDistanceStraight = 0.0;
        private double _laneDistanceCorner = 0.0;

        #endregion

        #region Constructor
        /// <summary>
        /// Initializes a new instance of the <see cref="Bot"/> class.
        /// </summary>
        /// <param name="host">Server hostname</param>
        /// <param name="port">Server port</param>
        /// <param name="name">The name of the bot.</param>
        /// <param name="key">Team key for the bot.</param>
        public Bot(string host, int port, string name, string key, bool diagnostics)
        {
            // Set bot parameters
            _logger = new Logger(".\\logs\\" + name + "_session_statistics");
            _client = new GameClient(name, host, port);
            Name = name;
            _accessKey = key;

            // Allocate event functionality
            _client.GameStatusChanged += OnGameStatusChanged;
            _client.RaceInitialized += OnRaceInitialized;
            _client.GameTickUpdated += OnGameTickUpdated;

            // Initialize variables
            TrackSections = new List<TrackSection>();

            // Enable diagnostic view
            if (diagnostics) Diagnostics.Register(this);
        }
        #endregion

        #region Game control methods
        /// <summary>
        /// Fast joins the bot into a solo game.
        /// </summary>
        public void FastJoinGame()
        {
            _client.JoinGame(Name, _accessKey);
        }

        public void CreateGame(string trackId, int numCars)
        {
            _client.CreateGame(Name, _accessKey, trackId, numCars);
        }

        public void CreateGame(string trackId, string password, int numCars)
        {
            _client.CreateGame(Name, _accessKey, trackId, password, numCars);
        }

        /// <summary>
        /// Joins the specified game.
        /// </summary>
        /// <param name="numCars">The number cars.</param>
        public void JoinGame(string track, int numCars)
        {
            _client.JoinGame(Name, _accessKey, track, numCars);
        }

        /// <summary>
        /// Joins the specified game with password.
        /// </summary>
        /// <param name="track">The track.</param>
        /// <param name="password">The password.</param>
        /// <param name="numCars">The number cars.</param>
        public void JoinGame(string track, string password, int numCars)
        {
            _client.JoinGame(Name, _accessKey, track, password, numCars);
        }

        /// <summary>
        /// Tells the client to set the throttle on next available tick.
        /// </summary>
        /// <param name="value">The value.</param>
        public void SetThrottle(double value)
        {
            _client.SetThrottle(value);
        }

        /// <summary>
        /// Tells the client to set the throttle on specific tick.
        /// </summary>
        /// <param name="value">The value.</param>
        /// <param name="tick">The tick.</param>
        public void SetThrottle(double value, int tick)
        {
            _client.SetThrottle(value, tick);
        }

        /// <summary>
        /// Switches the lanes.
        /// </summary>
        /// <param name="direction">The direction.</param>
        public void SwitchLanes(Direction direction)
        {
            _client.SwitchLanes(direction);
        }
        #endregion

        #region AI logic

        public double[] CalculatePieceLengthForSection(List<TrackLane> trackLanes, SectionType sectionType)
        {
            double[] sectionLanes = new double[trackLanes.Count];

            switch (sectionType)
            {
                case SectionType.Straight:
                    for (int i = 0; i < trackLanes.Count; i++)
                    {
                        sectionLanes[i] = 100.0;
                    }
                    break;
                case SectionType.Corner:
                    for (int i = 0; i < trackLanes.Count; i++)
                    {
                        // TODO Calculate lane length correctly from radius etc
                        sectionLanes[i] = 100.0;
                    }
                    break;
                case SectionType.Switch:
                    for (int i = 0; i < trackLanes.Count; i++)
                    {
                        // TODO Calculate lane length
                        sectionLanes[i] = 100.0;
                    }
                    break;
                case SectionType.CornerSwitch:
                    for (int i = 0; i < trackLanes.Count; i++)
                    {
                        // TODO Calculate lane length
                        sectionLanes[i] = 100.0;
                    }
                    break;
                default:
                    throw new ArgumentOutOfRangeException("sectionType");
            }

            return sectionLanes;
        }

        public void MapTrackToSections(Track trackData)
        {
            List<TrackLane> trackLanes = trackData.lanes;

            foreach (TrackPiece trackPiece in trackData.pieces)
            {
                // Process the first section
                if (TrackSections.Count == 0)
                {
                    if (trackPiece.@switch)
                    {
                        if (Math.Abs(trackPiece.angle) > 0)
                        {
                            double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.CornerSwitch);

                            List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                            TrackSections.Add(new TrackSection()
                            {
                                SectionType = SectionType.CornerSwitch,
                                SectionLanes = sectionLanes
                            });
                        }
                        else
                        {
                            double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Switch);

                            List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                            TrackSections.Add(new TrackSection()
                            {
                                SectionType = SectionType.Switch,
                                SectionLanes = sectionLanes
                            });
                        }
                    }
                    else if (Math.Abs(trackPiece.angle) > 0)
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Corner);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.Corner,
                            SectionLanes = sectionLanes
                        });
                    }
                    else
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Straight);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.Straight,
                            SectionLanes = sectionLanes
                        });
                    }

                    continue;
                }
                // Map switch piece to list
                if (trackPiece.@switch)
                {
                    if (Math.Abs(trackPiece.angle) > 0)
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.CornerSwitch);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.CornerSwitch,
                            SectionLanes = sectionLanes
                        });
                    }
                    else
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Switch);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.Switch,
                            SectionLanes = sectionLanes
                        });
                    }
                }
                // Map sequential corner pieces to list
                else if (Math.Abs(trackPiece.angle) > 0)
                {
                    // Append to currently processed section if it's a corner
                    if (TrackSections[TrackSections.Count - 1].SectionType == SectionType.Corner)
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Corner);

                        for (int index = 0; index < TrackSections[TrackSections.Count - 1].SectionLanes.Count; index++)
                        {
                            SectionLane sectionLane = TrackSections[TrackSections.Count - 1].SectionLanes[index];
                            sectionLane.Length += laneLengths[index];
                        }
                    }
                    // Else create a new section for corner
                    else
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Corner);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.Corner,
                            SectionLanes = sectionLanes
                        });
                    }
                }
                // Map sequential straights to list
                else
                {
                    // Append to currently processed section if it's a straight
                    if (TrackSections[TrackSections.Count - 1].SectionType == SectionType.Straight)
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Straight);

                        for (int index = 0; index < TrackSections[TrackSections.Count - 1].SectionLanes.Count; index++)
                        {
                            SectionLane sectionLane = TrackSections[TrackSections.Count - 1].SectionLanes[index];
                            sectionLane.Length += laneLengths[index];
                        }
                    }
                    // Else create a new section for straight
                    else
                    {
                        double[] laneLengths = CalculatePieceLengthForSection(trackLanes, SectionType.Straight);

                        List<SectionLane> sectionLanes = laneLengths.Select(t => new SectionLane() { Length = t }).ToList();

                        TrackSections.Add(new TrackSection()
                        {
                            SectionType = SectionType.Straight,
                            SectionLanes = sectionLanes
                        });
                    }
                }
            }
        }

        // Helper to get the ID:s of the next two pieces
        // TODO: Generalize to get the next n pieces or a list of n pieces?
        public void GetNextPieces()
        {
            if (Car.piecePosition.pieceIndex == Race.track.pieces.Count - 1)
            {
                _nextPiece = 0;
                _nextPiece2 = 1;
            }
            else if (Car.piecePosition.pieceIndex == Race.track.pieces.Count - 2)
            {
                _nextPiece = Race.track.pieces.Count - 1;
                _nextPiece2 = 0;
            }
            else
            {
                _nextPiece = (Car.piecePosition.pieceIndex + 1);
                _nextPiece2 = (Car.piecePosition.pieceIndex + 2);
            }
        }

        // Logic to initiate lane switching
        // Tries to always take the inner lane, only plans two pieces ahead
        // TODO: Plan further ahead, or separate logic from switching
        public void LaneSwitchLogic()
        {
            if (Race.track.pieces[_nextPiece].@switch && Race.track.pieces[_nextPiece2].angle != 0.0)
            {
                //_race.track.lanes.Count;
                //_car.piecePosition.lane.startLaneIndex;
                //_car.piecePosition.lane.endLaneIndex;
                if (Car.piecePosition.lane.startLaneIndex == _targetTrackLane)
                {

                    if (Race.track.pieces[_nextPiece2].angle > 0.0 && Race.track.lanes.Count - 1 > Car.piecePosition.lane.startLaneIndex)
                    {
                        SwitchLanes(Direction.Right);
                        _targetTrackLane = _targetTrackLane + 1;
                    }
                    else if (Race.track.pieces[_nextPiece2].angle < 0.0 && 0 < Car.piecePosition.lane.startLaneIndex)
                    {
                        SwitchLanes(Direction.Left);
                        _targetTrackLane = _targetTrackLane - 1;
                    }

                }

            }
            /* Dafuq - This algorithm really doesn't work well
             *          if (Car.piecePosition.lane.startLaneIndex == _targetTrackLane)
                        {
                            if (Car.piecePosition.lane.startLaneIndex == _targetTrackLane)
                            {
                                if (Race.track.pieces[_lastPieceId].angle > 0.0 && 0 < Car.piecePosition.lane.startLaneIndex)
                                {
                                    SwitchLanes(Direction.Left);
                                    _targetTrackLane = _targetTrackLane - 1;
                                }
                                else if (Race.track.pieces[_lastPieceId].angle > 0.0 && Race.track.lanes.Count - 1 > Car.piecePosition.lane.startLaneIndex)
                                {
                                    SwitchLanes(Direction.Right);
                                    _targetTrackLane = _targetTrackLane + 1;
                                }
                    }
                        }*/
        }


        // Throttle adujustment based on the current and upcoming pieces
        // In dire need of rework to be based on geometry and speed
        // 
        /*
        public void AdjustThrottle()
        {
            if (_currentSpeed == 0.0)
            {
                Throttle = 1.0;
            }
            if (Car.piecePosition.pieceIndex != _currentPiece)
            {
                _currentPiece = Car.piecePosition.pieceIndex;

                if (Race.track.pieces[_nextPiece].angle == 0.0 && Race.track.pieces[_nextPiece2].angle == 0.0)
                {
                    Throttle = 0.7;
                }
                else if (Race.track.pieces[_nextPiece].angle != 0.0 && Throttle >= 0.6)
                {
                    if (_currentSpeed > 6.0)
                    {
                        Throttle = Throttle - 0.2 - _throttleAdjust;

                    }
                    else
                    {
                        Throttle = Throttle - 0.1 - _throttleAdjust;
                    }
                }
                else if (Race.track.pieces[Car.piecePosition.pieceIndex].angle != 0.0)
                {
                    if (Car.angle > 25.0 && _currentSpeed > 5.0 )
                    {
                        Throttle = Throttle - 0.3 - _throttleAdjust;
                    }
                    else if (Car.angle < 25.0 && _currentSpeed < 5.0)
                    {
                        Throttle = Throttle + 0.2;
                    }
                    else
                    {
                        // I guess everythings allrite
                    }

                }
                else
                {
                    Throttle = 0.6;
                }
            }
            else
            {
                //SetThrottle(_lastThrottle);
            }
        }

        */

        // Let's get dangerous!
        // (this doesn't work at all but should visualize what I was trying to do)
        private void AdjustThrottle() 
        {
            if (Race.track.pieces[_currentPiece].length != 0.0) 
            {
                if (distToCorner() < 150.0 + (20 * _frictionNumber)) 
                {
                    if ( (Math.Sqrt(CornerRadius(1)  * _frictionNumber) > _currentSpeed  ) )
                    {
                        Throttle = 1.5 * _frictionNumber;
                    }
                    else
                    {
                        Throttle = 0.2;
                    }
                }
                else
                {
                    Throttle = 1.0;
                }
            }
            else
            {
                if ( (Math.Sqrt( Race.track.pieces[_currentPiece].radius * _frictionNumber) > _currentSpeed ) )
                {
                    if (Car.angle <= 5.0 )
                    {
                        if(Throttle < 0.6)
                        {
                             Throttle = Throttle;
                        }
                        else
                        {
                            Throttle = 0.5;
                        }
                    }
                    else if(Car.angle > 10.0 && Car.angle < 25.0)
                    {
                        Throttle = _frictionNumber;
                        _frictionNumber = _frictionNumber - 0.05;
                    }
                    else
                    {
                        Throttle = 0.1;
                    }
                }
                else
                {
                    Throttle = Throttle;
                }

            }
        }

        private double CornerRadius(int n) 
        { 
            double _minRadius = 1000.0;
            int i = _currentPiece + n;
            if (i == Race.track.pieces.Count) 
            {
                i = 0;
            }

            while (Race.track.pieces[i].radius == 0.0)
            {
                i++;
                if (i == Race.track.pieces.Count) 
                {
                    i = 0;
                }
            }
            while (Race.track.pieces[i].radius != 0.0) 
            {
                if (Race.track.pieces[i].radius < _minRadius) 
                {
                    _minRadius = Race.track.pieces[i].radius;
                }
                i++;
                if (i == Race.track.pieces.Count)
                {
                    i = 0;
                }
            }
            return _minRadius * 2;
        }

        // How far!
        private double distToCorner() 
        {
            int i = _currentPiece;
            double _totalDist = 0.0;
            while (Race.track.pieces[i].radius == 0.0) 
            {
                _totalDist = _totalDist + Race.track.pieces[i].length;
                i++;
                if (i == Race.track.pieces.Count)
                {
                    i = 0;
                }
            }
            _totalDist = _totalDist - Car.piecePosition.inPieceDistance;
            return _totalDist;
        }


        private double distToStraight()
        {
            int i = _currentPiece;
            double _totalDist = 0.0;
            while (Race.track.pieces[i].radius != 0.0)
            {
                _totalDist = _totalDist + ((Race.track.pieces[i].radius - Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter)
                                          * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI);
                i++;
                if (i == Race.track.pieces.Count)
                {
                    i = 0;
                }
            }
            _totalDist = _totalDist - Car.piecePosition.inPieceDistance;
            return _totalDist;
        }


        // Calculate current speed from the position of the car
        private void CalculateSpeed()
        {
            _lastSpeed = _currentSpeed;
            

            if (Car.piecePosition.lane.startLaneIndex != Car.piecePosition.lane.endLaneIndex) 
            {
                _switchedLanes = true;
                double _laneDistanceTemp = (Math.Abs(Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter) + Math.Abs(Race.track.lanes[Car.piecePosition.lane.endLaneIndex].distanceFromCenter));
                _laneDistanceCorner = _laneDistanceTemp * 1.025 / 2 ;
                _laneDistanceStraight = _laneDistanceTemp / 10;

                if(Car.piecePosition.lane.startLaneIndex < Car.piecePosition.lane.endLaneIndex) 
                {
                    _switchedIn = true;
                }
                else
                {
                    _switchedIn = false;
                }
            }

            // More calc. needed if not on same piece
            if (_currentPiece != Car.piecePosition.pieceIndex)
            {
                // Check for first piece special case
                if (Car.piecePosition.pieceIndex == 0)
                {
                    _lastPieceId = Race.track.pieces.Count - 1;
                }
                else
                {
                    _lastPieceId = Car.piecePosition.pieceIndex - 1;
                }

                // Check if last piece was a corner (no length, only angle and radius)
                if (Race.track.pieces[_lastPieceId].angle != 0.0)
                {
                    // If switched lanes
                    if (_switchedLanes )
                    {
                        // Differing calcs depending on corner
                        if (Race.track.pieces[_lastPieceId].angle > 0.0)
                        {
                            // Take into account laneswitching
                            if (_switchedIn)
                            {
                                _currentSpeed = Car.piecePosition.inPieceDistance + ((Race.track.pieces[_lastPieceId].radius - Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter)
                                                * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) + _laneDistanceCorner - _lastinPieceDistance;
                            }
                            else
                            {
                                _currentSpeed = Car.piecePosition.inPieceDistance + ((Race.track.pieces[_lastPieceId].radius - Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter)
                                                * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) - _lastinPieceDistance; 
                            }
                        }
                        else
                        {
                            if (_switchedIn)
                            {
                                _currentSpeed = Car.piecePosition.inPieceDistance + 
                                                ((Race.track.pieces[_lastPieceId].radius + Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter )
                                                 * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) - _lastinPieceDistance; 
                            }
                            else 
                            {
                                _currentSpeed = Car.piecePosition.inPieceDistance + 
                                                ((Race.track.pieces[_lastPieceId].radius + Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter  )
                                                * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) + _laneDistanceCorner - _lastinPieceDistance;
                            }
                        }
                    }
                    else 
                    {
                        if (Race.track.pieces[_lastPieceId].angle > 0.0)
                        {
                            _currentSpeed = Car.piecePosition.inPieceDistance + ((Race.track.pieces[_lastPieceId].radius - Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter)
                                            * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) + _laneDistanceStraight - _lastinPieceDistance;
                        }
                        else
                        {
                            _currentSpeed = Car.piecePosition.inPieceDistance + ((Race.track.pieces[_lastPieceId].radius + Race.track.lanes[Car.piecePosition.lane.startLaneIndex].distanceFromCenter)
                                            * 2 * (Math.Abs(Race.track.pieces[_lastPieceId].angle) / 360) * Math.PI) + _laneDistanceStraight - _lastinPieceDistance;
                        }
                    }

                }
                
                // Last piece was a straight
                else
                {
                    // Check if we switched lanes
                    if(_switchedLanes && (Car.piecePosition.lane.startLaneIndex == Car.piecePosition.lane.endLaneIndex) )
                    {
                        _currentSpeed = Car.piecePosition.inPieceDistance + (Race.track.pieces[_lastPieceId].length - _lastinPieceDistance) + _laneDistanceStraight;
                    }
                    else
                    {
                        _currentSpeed = Car.piecePosition.inPieceDistance + (Race.track.pieces[_lastPieceId].length - _lastinPieceDistance);

                    }
                }

                // Common for all options 
                _lastinPieceDistance = Car.piecePosition.inPieceDistance;

                // Reset lane-switch modifiers
                if (_switchedLanes)  
                {
                    _switchedLanes = false;
                    _laneDistanceStraight = 0.0;
                    _laneDistanceCorner = 0.0;
                }
            }

            // If we stay on the same piece the calculations are simpler
            else
            {
                _currentSpeed = Car.piecePosition.inPieceDistance - _lastinPieceDistance;
                _lastinPieceDistance = Car.piecePosition.inPieceDistance;
            }
        }

        // Calculate Acceleration based on the two previous speeds
        private void CalculateAcc()
        {
            _lastAcceleration = _currentAcceleration;
            _currentAcceleration = _currentSpeed - _lastSpeed;
        }

        // Getter for current speed
        public double GetCurrentSpeed()
        {
            return _currentSpeed;
        }

        // Getter for current acceleration
        public double GetCurrentAcceleration()
        {
            return _currentAcceleration;
        }


        #endregion

        #region Event handlers
        /// <summary>
        /// Listens the gameclient status changes.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
        /// <exception cref="System.NotImplementedException"></exception>
        private void OnGameStatusChanged(object sender, GameClientEventArgs args)
        {
            MessageWrapper messageWrapper = args.ArgumentData as MessageWrapper;
            string timestamp = DateTime.Now.ToString("HH:MM:ss") + ' ';

            switch (args.GameState)
            {
                case GameState.Error:
                    GameState = GameState.Error;
                    Diagnostics.Notify(timestamp + Name + " Error: " + ObjectDumper.Dump(messageWrapper));
                    break;
                case GameState.Created:
                    GameState = GameState.Created;
                    Diagnostics.Notify(timestamp + Name + " has created the game");
                    break;
                case GameState.Joined:
                    GameState = GameState.Joined;
                    Diagnostics.Notify(timestamp + Name + " has joined the game");
                    break;
                case GameState.Started:
                    GameState = GameState.Started;
                    // TODO: replace with start logic
                    // Put the pedal to the medal
                    _botAlive = true;
                    _targetTrackLane = Car.piecePosition.lane.startLaneIndex;
                    Diagnostics.Notify(timestamp + "The race has started");
                    break;
                case GameState.Ended:
                    // TODO: Process race
                    GameState = GameState.Ended;
                    _botAlive = false;
                    Diagnostics.Notify(timestamp + "The race has ended");
                    break;
                case GameState.Crash:
                    if (messageWrapper != null)
                    {
                        JObject carIdJson = messageWrapper.data as JObject;

                        if (carIdJson != null)
                        {
                            CarIdentity carId = carIdJson.ToObject<CarIdentity>();
                            if (carId.name == Name)
                            {
                                GameState = GameState.Crash;
                                _frictionNumber = _frictionNumber - 0.05;
                                _botAlive = false;

                                Diagnostics.Notify(timestamp + Name + " has crashed.");
                            }
                        }
                    }
                    break;
                case GameState.Spawn:
                    if (messageWrapper != null)
                    {
                        JObject carIdJson = messageWrapper.data as JObject;

                        if (carIdJson != null)
                        {
                            CarIdentity carId = carIdJson.ToObject<CarIdentity>();
                            if (carId.name == Name)
                            {
                                GameState = GameState.Spawn;
                                _botAlive = true;

                                Diagnostics.Notify(timestamp + Name + " has respawned.");
                            }
                        }
                    }
                    _botAlive = true;
                    break;
                case GameState.Disqualified:
                    if (messageWrapper != null)
                    {
                        JObject disqualifyDetailJson = messageWrapper.data as JObject;

                        if (disqualifyDetailJson != null)
                        {
                            List<KeyValuePair<string, object>> disqualifyDetail = disqualifyDetailJson.ToObject<List<KeyValuePair<string, object>>>();

                            if (((CarIdentity) disqualifyDetail[0].Value).name == Name)
                            {
                                GameState = GameState.Disqualified;
                                _botAlive = false;
                                Throttle = 0.0;

                                Diagnostics.Notify(timestamp + Name + " has been disqualified due to " + (string)disqualifyDetail[1].Value);
                            }
                        }
                    }
                    break;
                case GameState.LapFinished:
                    // TODO: Add lap statistics
                    GameState = GameState.LapFinished;
                    Diagnostics.Notify(timestamp + Name + " finished a lap");
                    break;
                case GameState.Finished:
                    GameState = GameState.Finished;
                    _botAlive = false;
                    Diagnostics.Notify(timestamp + Name + " finished the race");
                    break;
                case GameState.TournamentEnd:
                    GameState = GameState.TournamentEnd;
                    Diagnostics.Notify(timestamp + "Tournament has ended");
                    TournamentEnded(this);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        /// <summary>
        /// Handles the initial race data for the bot.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
        private void OnRaceInitialized(object sender, GameClientEventArgs args)
        {
            // Update race data
            Race = (Race)args.ArgumentData;
            MapTrackToSections(Race.track);
        }

        /// <summary>
        /// Update race car position data.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="args">The <see cref="GameClientEventArgs"/> instance containing the event data.</param>
        private void OnGameTickUpdated(object sender, GameClientEventArgs args)
        {
            MessageWrapper messageWrapper = args.ArgumentData as MessageWrapper;
            if (messageWrapper != null)
            {
                JArray carDataJson = messageWrapper.data as JArray;

                if (carDataJson != null)
                {
                    List<Car> carData = carDataJson.ToObject<List<Car>>();
                    Race.cars = carData;
                    Car = carData.FirstOrDefault(c => c.id.name == Name);
                }

                // Need to sync this with server sent tick
                if (messageWrapper.gameTick != null)
                {
                    GameTick = messageWrapper.gameTick;
                }
            }

            if (_botAlive)
            {
                // Find the next track-pieces
                GetNextPieces();
                CalculateSpeed();
                CalculateAcc();
                // Check if we're on the optimal lane for the next corner
                LaneSwitchLogic();

                // Adjust our throttle (experimental)
                _currentPiece = Car.piecePosition.pieceIndex;
                AdjustThrottle();


                _logger.WriteLine(string.Join(";", new object[] { GameTick, Throttle, _currentSpeed, _currentAcceleration, Car.angle }));
            }
        }
        #endregion
    }
}
