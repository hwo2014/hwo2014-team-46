﻿using System;
using HelloWorldOpen.Utils;

namespace HelloWorldOpen
{
    class Program
    {
        private static string _host;
        private static int _port;
        private static int _numberOfBots;

        private static string _botName;
        private static string _botKey;

        /// <summary>
        /// Main entry method.
        /// 
        /// Required arguments:
        /// Host - Hostaddress or hostname for the server.
        /// Port - Server port.
        /// Name - Name of the bot.
        /// Key  - Access key for the bot.
        /// </summary>
        /// <param name="args">The arguments.</param>
        public static void Main(string[] args)
        {
            // Parse connection variables
            _host = args[0];
            _port = int.Parse(args[1]);
            // Parse bot variables
            _botName = args[2];
            _botKey = args[3];
            // Get the amount of bots to use in race
            if (args.Length > 4) int.TryParse(args[4], out _numberOfBots);
            else _numberOfBots = 1;
            // Check for diagnostics switch
            bool diagnostic = false;
            if (args.Length > 5 && args[5].Contains("console"))
            {
                // Setup diagnostics dashboard
                Diagnostics.SetupDashboard();
                diagnostic = true;
            }

            // Create the all-mighty Omnibot
            Bot omniBot = new Bot(_host, _port, _botName + 0, _botKey, diagnostic);
            omniBot.TournamentEnded += OnTournamentEnded;

            // Create multiplayer game instance and spawn specified number of bots
            if (_numberOfBots > 1)
            {
                omniBot.CreateGame("germany", _numberOfBots);

                for (int i = 1; i <= _numberOfBots; i++)
                {
                    Bot newBot = new Bot(_host, _port, _botName + i, _botKey, diagnostic);
                    newBot.JoinGame("germany", _numberOfBots);
                }
            }
            // Create a solo game instance with a single bot
            else
            {
                omniBot.FastJoinGame();
            }

            while (true)
            {
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.LeftArrow:
                        Diagnostics.ChangeBotDisplay(-1);
                        break;
                    case ConsoleKey.RightArrow:
                        Diagnostics.ChangeBotDisplay(1);
                        break;
                }
            }
        }

        private static void OnTournamentEnded(object sender)
        {
            Console.WriteLine("Tournament Ended");
        }
    }
}
